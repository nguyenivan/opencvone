#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <boost/filesystem.hpp>
#include <fstream>
#include <streambuf>
#include <numeric>


namespace fs = boost::filesystem;
using namespace cv;
using namespace std;

int KINECT_WIDTH = 512, KINECT_HEIGHT= 424;

template <typename Container>
struct compare_indirect_index
{
	const Container& container;
	compare_indirect_index(const Container& container) : container(container) { }
	bool operator () (size_t lindex, size_t rindex) const
	{
		return container[lindex] < container[rindex];
	}
};

Point3f depthScreenToWorld(int x, int y, float z, Mat camMatrix, Mat kinectDist) {
	float fx = (float)camMatrix.at<double>(0, 0);
	float fy = (float)camMatrix.at<double>(1, 1);
	float cx = (float)camMatrix.at<double>(0, 2);
	float cy = (float)camMatrix.at<double>(1, 2);
	return Point3f((x - cx) * z / fx, (float)(y - cy) * z / fy, z);
}


Mat bitwiseRemoval(Mat backgroundImage, Mat objectImage) {

	// Check if channels == 1 , e.g grayscale
	Mat backgroundGray;
	if (backgroundGray.channels() == 3) {
		cvtColor(backgroundImage, backgroundGray, CV_BGR2GRAY);
	}
	else {
		backgroundGray = backgroundImage;
	}
	Mat objectGray;
	if (objectImage.channels() == 3) {
		cvtColor(objectImage, objectGray, CV_BGR2GRAY);
	}
	else {
		objectGray = objectImage;
	}
	Mat diff; absdiff(backgroundGray, objectGray, diff);
	int diffThreshold = 50;
	int maxValue = 255;
	threshold(diff, diff, diffThreshold, maxValue, CV_THRESH_BINARY);

	return diff;
}

Mat depthRemoval(vector<ushort> rawDepth, int depthWidth, int depthHeight, Mat colorIm,
	Mat depthMatrix, Mat depthDist, Mat colorRVec, Mat colorTVec, Mat colorMatrix, Mat colorDist)
{
	float nearPlane = 1000; // in mm
	float farPlane = 1800; // in mm
	Mat depthMap = Mat(depthHeight, depthWidth, CV_16U, &rawDepth.front());

	double min, max;
	minMaxIdx(rawDepth, &min, &max);
	float scaleFactor = float( 255 / max);
	double threshNear = nearPlane * scaleFactor;
	double threshFar = farPlane * scaleFactor;
	Mat depthShow; depthMap.convertTo(depthShow, CV_8UC1, scaleFactor);
	//threshold
	Mat tnear, tfar;
	depthShow.copyTo(tnear);
	depthShow.copyTo(tfar);
	threshold(tnear, tnear, threshNear, 255, CV_THRESH_TOZERO);
	threshold(tfar, tfar, threshFar, 255, CV_THRESH_TOZERO_INV);
	depthShow = tnear & tfar;//or cvAnd(tnear,tfar,depthShow,NULL); to join the two thresholded images

	vector<vector<Point>> contours;
	findContours(depthShow, contours, 0, 1);

	int numContours = int(contours.size());
	if (numContours == 0) return Mat::zeros(colorIm.size(), CV_8UC1); // No contour found

	// compute bounding box and circle to exclude small blobs(non human) or do further filtering,etc.
	vector<vector<Point> > contours_poly(numContours);
	vector<Rect> boundRect(numContours);
	vector<Point2f> centers(numContours);
	vector<float> radii(numContours);
	for (int i = 0; i < numContours; i++){
		approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
		minEnclosingCircle(contours_poly[i], centers[i], radii[i]);
	}

	// find the biggest circle and pick it only
	vector <size_t> indices(radii.size(), 0);
	iota(indices.begin(), indices.end(), 0);  // found in <numeric>
	sort(indices.begin(), indices.end(), compare_indirect_index <decltype(radii)>(radii));
	int maxCntrIdx = int(indices.back());

	vector<Point3f> worldContour;
	float* lastZ = NULL;
	for (Point dp : contours[maxCntrIdx]) {
		int i = int(dp.x + (dp.y * depthWidth + dp.y));
		float z = rawDepth.at(i);
		if (z <nearPlane || z > farPlane) {
			if (lastZ == NULL) {
				z = (nearPlane + farPlane ) / 2;
				lastZ = new float;
			}
			else {
				z = *lastZ;
			}
		}
		//need to flip
		Point3f worldPoint = depthScreenToWorld(depthWidth - dp.x, dp.y, z, depthMatrix, depthDist);
		worldContour.push_back(worldPoint);
		*lastZ = z;
	}

	vector<Point2f> newContour;
	vector<vector<Point>> colorContours;
	projectPoints(worldContour, colorRVec, colorTVec, colorMatrix, colorDist, newContour);

	colorContours.push_back(vector<Point>());
	Mat(newContour).copyTo(colorContours[0]);
	Mat cntr = Mat::zeros(colorIm.size(), CV_8UC1);
	drawContours(cntr, colorContours , 0, Scalar(255, 0, 0), -1, 8);

	return cntr;
}


struct NameAndCorners {
	String fileName;
	vector<Point2f> corners;
};

Size boardSize(10, 7);

bool extractCorners2(String filePath, OutputArray corners, int scalePercent, bool loadGrayScale = false){

	Mat image = imread(filePath);
	bool ret = findChessboardCorners(image, boardSize, corners);
	return ret;
}

bool extractCorners(String filePath, OutputArray corners, int scalePercent, bool loadGrayScale = false){

	IplImage* image = loadGrayScale ? cvLoadImage(filePath.c_str(), CV_LOAD_IMAGE_GRAYSCALE) : cvLoadImage(filePath.c_str());
	IplImage* scaled = image;
	if (scalePercent < 100) {
		cvCreateImage
			(cvSize(image->width*scalePercent / 100, image->height*scalePercent / 100),
			image->depth, image->nChannels);

		//use cvResize to resize source to a destination image
		cvResize(image, scaled);
	}

	bool ret = findChessboardCorners(Mat(scaled), boardSize, corners);
	//bool ret = findChessboardCorners(Mat(scaled), boardSize, corners, CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
	//	+ CALIB_CB_FAST_CHECK);
	return ret;
}

void extractCorners(){
	vector<NameAndCorners> cornersCollection;
	String imageList = "dataset.yaml";
	String cornerList = "corners.yaml";
	FileStorage list(imageList, FileStorage::READ);
	FileStorage corners(cornerList, FileStorage::WRITE);
	String datasetFolderNode;
	list["datasetFolder"] >> datasetFolderNode;
	fs::path datasetFolder = fs::path(datasetFolderNode);
	vector<String> datasetImages;
	list["datasetImages"] >> datasetImages;
	corners << "datasetFolder" << datasetFolderNode;
	corners << "cornersCollection" << "[";
	for (vector<String>::iterator i = datasetImages.begin(); i != datasetImages.end(); ++i) {
		vector<Point2f> dslrCorners;
		vector<Point2f> kinectCorners;
		fs::path dslrImName(*i);
		if (++i == datasetImages.end()){
			break;
		}
		fs::path kinectImName(*i);
		cout << "Processing " << dslrImName << "...";
		int scalePercent = 25;
		bool foundDslr = extractCorners((datasetFolder / dslrImName).string(), dslrCorners, scalePercent, true);
		cout << (foundDslr ? " FOUND!\n" : " NOT FOUND!\n");
		cout << "Processing " << kinectImName << "...";
		bool foundKinect = extractCorners((datasetFolder / kinectImName).string(), kinectCorners, 100, false);
		cout << (foundKinect ? " FOUND!\n" : " NOT FOUND!\n");
		if (foundDslr && foundKinect) {
			//cornersCollection.push_back(NameAndCorners{ dslrImName.string(), dslrCorners });
			//cornersCollection.push_back(NameAndCorners{ kinectImName.string(), kinectCorners });
			corners << "{:" << "fileName" << dslrImName.string() << "corners" << dslrCorners << "}";
			corners << "{:" << "fileName" << kinectImName.string() << "corners" << kinectCorners << "}";
		}
	}
	corners << "]";
	//corners << "cornersCollection" << cornersCollection;
	list.release();
	corners.release();
}


void testCorners(String filePath){
	FileStorage cornersStorage(filePath, FileStorage::READ);
	String datasetFolder;
	cornersStorage["datasetFolder"] >> datasetFolder;
	FileNode cornersCollection = cornersStorage["cornersCollection"];
	FileNodeIterator i = cornersCollection.begin();
	FileNodeIterator e = cornersCollection.end();
	Size boardSize(10, 7);
	for (; i != e; ++i) {
		String dslrName = (*i)["fileName"];
		vector<Point2f> dslrCorners;
		(*i)["corners"] >> dslrCorners;

		if (++i == e) break;

		String kinectName = (*i)["fileName"];
		vector<Point2f> kinectCorners;
		(*i)["corners"] >> kinectCorners;

		Mat im1 = imread((fs::path(datasetFolder) / fs::path(dslrName)).string());
		drawChessboardCorners(im1, boardSize, dslrCorners, true);
		Mat im2 = imread((fs::path(datasetFolder) / fs::path(kinectName)).string());
		drawChessboardCorners(im2, boardSize, kinectCorners, true);
		Size sz1 = im1.size();
		Size sz2 = im2.size();
		Mat im3(sz1.height, sz1.width + sz2.width, CV_8UC3);
		Mat left(im3, Rect(0, 0, sz1.width, sz1.height));
		im1.copyTo(left);
		Mat right(im3, Rect(sz1.width, 0, sz2.width, sz2.height));
		im2.copyTo(right);

		String windowsName = "Processed Image";
		namedWindow(windowsName, CV_WINDOW_NORMAL);
		imshow(windowsName, im3);

		Size screenSize(1280, 720);
		Size imageSize = im3.size();
		float scale = fmin((float)(screenSize.width / imageSize.width), (float)(screenSize.height / imageSize.height));
		scale = fmax(scale, 1.0f);
		cv::resizeWindow(windowsName, int(scale * imageSize.width), int(scale * imageSize.height));
		waitKey(1000);
	}
	cornersStorage.release();
	waitKey(10000);
}

// squareSize is in milimeter
vector<Point3f> initObjPoints(Size size, float squareSize = 1.0f){
	vector<Point3f> obj;
	for (int j = 0; j < size.width * size.height; j++)
		obj.push_back(Point3f((j / size.width) * squareSize, (j%size.width) * squareSize, 0.0f));
	return obj;
}

vector<ushort> readCsv(String fileName) {

	std::ifstream t(fileName);
	std::string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());

	std::vector<ushort> vect;
	std::stringstream ss(str);
	ushort i;
	while (ss >> i)
	{
		vect.push_back(i);
		if (ss.peek() == ',' || ss.peek() == ' ')
			ss.ignore();
	}
	return vect;
}

void filterDot(){
	ushort threshold = 2000;
	cout << "Loading calibration ...\n";
	String calibFile = "calibration.yaml";
	FileStorage calibStorage(calibFile, FileStorage::READ);
	Mat dslrMatrix, kinectMatrix, dslrDist, kinectDist, dslrRvec, dslrTvec;
	calibStorage["dslrMatrix"] >> dslrMatrix;
	calibStorage["dslrDist"] >> dslrDist;
	calibStorage["kinectMatrix"] >> kinectMatrix;
	calibStorage["kinectDist"] >> kinectDist;
	calibStorage["dslrRvec"] >> dslrRvec;
	calibStorage["dslrTvec"] >> dslrTvec;
	calibStorage.release();

	vector<NameAndCorners> cornersCollection;
	String imageList = "dataset.yaml";
	FileStorage list(imageList, FileStorage::READ);
	String datasetFolderNode;
	list["datasetFolder"] >> datasetFolderNode;
	fs::path datasetFolder = fs::path(datasetFolderNode);
	vector<String> datasetImages;
	list["datasetImages"] >> datasetImages;

	String windowsName = "Processed Image";
	namedWindow(windowsName, CV_WINDOW_NORMAL);

	for (vector<String>::iterator i = datasetImages.begin(); i != datasetImages.end(); ++i) {
		fs::path dslrImName(*i);
		if (++i == datasetImages.end()){
			break;
		}
		fs::path kinectImName(*i);
		cout << "Processing " << dslrImName << "...";
		auto imExt = fs::extension(dslrImName);
		auto imBase = fs::basename(dslrImName);
		String depthSuffix = "_depth";
		String depthExt = ".csv";
		auto depthImName = datasetFolder / (imBase + depthSuffix + depthExt);
		vector<ushort> depthPoints = readCsv(depthImName.string());
		int count = 0, kinectWidth = 512, kinectHeight = 424;

		vector<Point3f> toRemoveWorld;
		for (auto z : depthPoints) {
			if (z < threshold) {
				// Need to flip
				int x = kinectWidth - (count % kinectWidth);
				int y = count / kinectWidth;
				Point3f worldPoint = depthScreenToWorld(x, y, z, kinectMatrix, kinectDist);
				toRemoveWorld.push_back(worldPoint);
			}
			count++;
		}

		vector<Point2f> toRemoveDslr;
		projectPoints(toRemoveWorld, dslrRvec, dslrTvec, dslrMatrix, dslrDist, toRemoveDslr);
		Point dslrCenter(int(dslrMatrix.at<double>(0, 2)), int(dslrMatrix.at<double>(1, 2)));
		Mat dslrIm = imread((datasetFolder / dslrImName).string(), CV_LOAD_IMAGE_COLOR);

		Mat dslrMask = Mat::zeros(dslrIm.size(), CV_8UC1);
		int magnitude = 10;
		for (auto p : toRemoveDslr) {
			int flippedX = int(p.x);
			int flippedY = int(p.y);
			if (0 <= flippedX && flippedX < dslrIm.size().width && 0 <= flippedY && flippedY < dslrIm.size().height){
				circle(dslrMask, Point(flippedX, flippedY), magnitude, 1, CV_FILLED);
				//dslrMask.at<uchar>(Point(int(p.x), int(p.y)))= 1;
			}
		}

		Mat dslrWithoutBg = Mat::zeros(dslrIm.size(), CV_8UC3);
		dslrIm.copyTo(dslrWithoutBg, dslrMask);

		imshow(windowsName, dslrMask);

		/*	String procSuffix = "_processed";
		auto procImName = datasetFolder / (imBase + procSuffix + imExt);
		cout << "Writing " << procImName.string() << " ...";
		imwrite(procImName.string(), dslrWithoutBg);*/
		cout << " Done!\n";
		waitKey(5000);
	}
	list.release();

	waitKey(10000);
}

void on_trackbar(int, void*){}

void filterBackground(){
	vector<NameAndCorners> cornersCollection;
	String imageList = "dataset.yaml";
	FileStorage list(imageList, FileStorage::READ);
	String datasetFolderNode;
	list["datasetFolder"] >> datasetFolderNode;
	fs::path datasetFolder = fs::path(datasetFolderNode);
	fs::path backgroundImName("Background.jpg");
	Mat backgroundImGray = imread((datasetFolder / backgroundImName).string(), 0);
	vector<String> datasetImages;
	list["datasetImages"] >> datasetImages;
	list.release();
	namedWindow("diff", CV_WINDOW_NORMAL);
	for (vector<String>::iterator i = datasetImages.begin();;) {
		fs::path dslrImName(*i);
		cout << "Processing " << dslrImName << "...";

		Mat dslrImGray = imread((datasetFolder / dslrImName).string(), 0);
		cout << "Color chanels: " << dslrImGray.channels();

		Mat diff = bitwiseRemoval(backgroundImGray, dslrImGray);
		imshow("diff", diff);

		std::cout << " Done!\n";
		if (++i == datasetImages.end()){
			i = datasetImages.begin(); // Need this to read depth image after Dslr image
		}
		else if (++i == datasetImages.end()){
				i = datasetImages.begin(); // Need this to read depth image after Dslr image
		}

		Size screenSize(1280, 720);
		Size imageSize = diff.size();
		float scale = fmin((float)(screenSize.width / imageSize.width), (float)(screenSize.height / imageSize.height));
		scale = fmax(scale, 1.0f);
		cv::resizeWindow("diff", int(scale * imageSize.width), int(scale * imageSize.height));

		if (waitKey() == 27) break;//exit on 
	}
	destroyAllWindows();
}


void filterContour(){
	cout << "Loading calibration ...\n";
	String calibFile = "calibration.yaml";
	FileStorage calibStorage(calibFile, FileStorage::READ);
	Mat dslrMatrix, kinectMatrix, dslrDist, kinectDist, dslrRvec, dslrTvec;
	calibStorage["dslrMatrix"] >> dslrMatrix;
	calibStorage["dslrDist"] >> dslrDist;
	calibStorage["kinectMatrix"] >> kinectMatrix;
	calibStorage["kinectDist"] >> kinectDist;
	calibStorage["dslrRvec"] >> dslrRvec;
	calibStorage["dslrTvec"] >> dslrTvec;
	calibStorage.release();

	vector<NameAndCorners> cornersCollection;
	String imageList = "dataset.yaml";
	FileStorage list(imageList, FileStorage::READ);
	String datasetFolderNode;
	list["datasetFolder"] >> datasetFolderNode;
	fs::path datasetFolder = fs::path(datasetFolderNode);
	vector<String> datasetImages;
	list["datasetImages"] >> datasetImages;
	list.release();

	int threshNear = 60;
	int threshFar = 90;
	int dilateAmt = 20;
	int erodeAmt = 20;
	int blurAmt = 20;
	int blurPre = 20;

	vector<vector<Point> > contours;
	namedWindow("depth map");
	namedWindow("contours", CV_WINDOW_NORMAL);

	createTrackbar("dilate", "depth map", &dilateAmt, 16, on_trackbar);
	createTrackbar("erode", "depth map", &erodeAmt, 16, on_trackbar);
	createTrackbar("blur", "depth map", &blurAmt, 16, on_trackbar);
	createTrackbar("blurpre", "depth map", &blurPre, 1, on_trackbar);
	createTrackbar("near", "depth map", &threshNear, 255, on_trackbar);
	createTrackbar("far", "depth map", &threshFar, 255, on_trackbar);
	for (vector<String>::iterator i = datasetImages.begin();; ++i) {
		fs::path dslrImName(*i);
		if (++i == datasetImages.end()){
			i = datasetImages.begin(); // Need this to read depth image after Dslr image
		}
		fs::path kinectImName(*i);
		cout << "Processing " << dslrImName << "...";
		Mat dslrIm = imread((datasetFolder / dslrImName).string(), CV_LOAD_IMAGE_COLOR);
		auto imExt = fs::extension(dslrImName);
		auto imBase = fs::basename(dslrImName);
		String depthSuffix = "_depth";
		String depthExt = ".csv";
		auto depthImName = datasetFolder / (imBase + depthSuffix + depthExt);
		vector<ushort> depthPoints = readCsv(depthImName.string());
		double min, max;
		int kinectWidth = 512, kinectHeight = 424;
		Mat depthMap = Mat(kinectHeight, kinectWidth, CV_16U, &depthPoints.front());
		minMaxIdx(depthPoints, &min, &max);
		const float scaleFactor = 0.05f;

		Mat depthShow; depthMap.convertTo(depthShow, CV_8UC1, scaleFactor);
		//threshold
		Mat tnear, tfar;
		depthShow.copyTo(tnear);
		depthShow.copyTo(tfar);
		threshold(tnear, tnear, threshNear, 255, CV_THRESH_TOZERO);
		threshold(tfar, tfar, threshFar, 255, CV_THRESH_TOZERO_INV);
		depthShow = tnear & tfar;//or cvAnd(tnear,tfar,depthShow,NULL); to join the two thresholded images

		findContours(depthShow, contours, 0, 1);


		//optionally compute bounding box and circle to exclude small blobs(non human) or do further filtering,etc.
		int numContours = int(contours.size());
		if (numContours == 0) continue;
		vector<vector<Point> > contours_poly(numContours);
		vector<Rect> boundRect(numContours);
		vector<Point2f> centers(numContours);
		vector<float> radii(numContours);
		for (int i = 0; i < numContours; i++){
			approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
			boundRect[i] = boundingRect(Mat(contours_poly[i]));
			minEnclosingCircle(contours_poly[i], centers[i], radii[i]);
		}

		vector <size_t> indices(radii.size(), 0);
		iota(indices.begin(), indices.end(), 0);  // found in <numeric>
		sort(indices.begin(), indices.end(), compare_indirect_index <decltype(radii)>(radii));
		int maxCntrIdx = int(indices.back());

		vector<Point3f> worldContour;
		float* lastZ = NULL;
		for (Point dp : contours[maxCntrIdx]) {
			int i = int(dp.x + (dp.y * kinectWidth + dp.y));
			float z = depthPoints.at(i);
			float scaledZ = z * scaleFactor;
			if (scaledZ <threshNear || scaledZ > threshFar) {
				if (lastZ == NULL) {
					z = (threshNear + threshFar) / (scaleFactor * 2);
					lastZ = new float;
				}
				else {
					z = *lastZ;
				}
			}
			//need to flip
			Point3f worldPoint = depthScreenToWorld(kinectWidth - dp.x, dp.y, z, kinectMatrix, kinectDist);
			worldContour.push_back(worldPoint);
			*lastZ = z;
		}

		vector<vector<Point>> dslrContours;
		vector<Point2f> newContour;
		dslrContours.push_back(vector<Point>());
		projectPoints(worldContour, dslrRvec, dslrTvec, dslrMatrix, dslrDist, newContour);

		Mat curveContour;
		approxPolyDP(newContour, curveContour, 3, true);


		curveContour.copyTo(dslrContours[0]);
		Mat cntr = Mat::zeros(dslrIm.size(), CV_8UC1);
		drawContours(cntr, dslrContours, 0, Scalar(255, 0, 0), -1, 8);

		//filter
		if (blurPre == 1) blur(cntr, cntr, Size(blurAmt + 1, blurAmt + 1));
		Mat dslrMask; cntr.copyTo(dslrMask);
		erode(dslrMask, dslrMask, Mat(), Point(-1, -1), erodeAmt);
		if (blurPre == 0) blur(dslrMask, dslrMask, Size(blurAmt + 1, blurAmt + 1));
		dilate(dslrMask, dslrMask, Mat(), Point(-1, -1), dilateAmt);
		Mat show;
		dslrIm.copyTo(show, dslrMask);
		imshow("depth map", depthShow);
		imshow("contours", show);

		Size screenSize(1280, 720);
		Size imageSize = show.size();
		float scale = fmin((float)(screenSize.width / imageSize.width), (float)(screenSize.height / imageSize.height));
		scale = fmax(scale, 1.0f);
		cv::resizeWindow("contours", int(scale * imageSize.width), int(scale * imageSize.height));
		std::cout << " Done!\n";
		if (waitKey(30) == 27) break;//exit on esc
	}
}
void filterCombine(){
	cout << "Loading calibration ...\n";
	String calibFile = "calibration.yaml";
	FileStorage calibStorage(calibFile, FileStorage::READ);
	Mat dslrMatrix, kinectMatrix, dslrDist, kinectDist, dslrRvec, dslrTvec;
	calibStorage["dslrMatrix"] >> dslrMatrix;
	calibStorage["dslrDist"] >> dslrDist;
	calibStorage["kinectMatrix"] >> kinectMatrix;
	calibStorage["kinectDist"] >> kinectDist;
	calibStorage["dslrRvec"] >> dslrRvec;
	calibStorage["dslrTvec"] >> dslrTvec;
	calibStorage.release();

	vector<NameAndCorners> cornersCollection;
	String imageList = "dataset.yaml";
	FileStorage list(imageList, FileStorage::READ);
	String datasetFolderNode;
	list["datasetFolder"] >> datasetFolderNode;
	fs::path datasetFolder = fs::path(datasetFolderNode);
	vector<String> datasetImages;
	list["datasetImages"] >> datasetImages;
	list.release();

	fs::path backgroundImName("Background.jpg");
	Mat backgroundImGray = imread((datasetFolder / backgroundImName).string(), 0);

	int threshNear = 60;
	int threshFar = 90;
	int dilateAmt = 50;
	int erodeAmt = 5;
	int blurAmt = 20;
	int blurPre = 20;

	vector<vector<Point> > contours;
	namedWindow("contours", CV_WINDOW_NORMAL);
	namedWindow("ref", CV_WINDOW_NORMAL);
	Size screenSize(1280, 720);

	createTrackbar("dilate", "depth map", &dilateAmt, 16, on_trackbar);
	createTrackbar("erode", "depth map", &erodeAmt, 16, on_trackbar);
	createTrackbar("blur", "depth map", &blurAmt, 16, on_trackbar);
	createTrackbar("blurpre", "depth map", &blurPre, 1, on_trackbar);
	createTrackbar("near", "depth map", &threshNear, 255, on_trackbar);
	createTrackbar("far", "depth map", &threshFar, 255, on_trackbar);

	for (vector<String>::iterator i = datasetImages.begin();;) {
		fs::path dslrImName(*i);
		if (++i == datasetImages.end()){
			i = datasetImages.begin(); // Need this to read depth image after Dslr image
			break;
		}
		fs::path kinectImName(*i);
		cout << "Processing " << dslrImName << "...";
		Mat dslrIm = imread((datasetFolder / dslrImName).string(), CV_LOAD_IMAGE_COLOR);
		auto imExt = fs::extension(dslrImName);
		auto imBase = fs::basename(dslrImName);
		String depthSuffix = "_depth";
		String depthExt = ".csv";
		auto depthImName = datasetFolder / (imBase + depthSuffix + depthExt);
		vector<ushort> depthPoints = readCsv(depthImName.string());
		
		Mat baseMask = depthRemoval(depthPoints, KINECT_WIDTH, KINECT_HEIGHT, dslrIm, kinectMatrix, kinectDist, dslrRvec, dslrTvec, dslrMatrix, dslrDist);

		Mat minMask; erode(baseMask, minMask, Mat(), Point(-1, -1), erodeAmt);
		Mat roiMask; dilate(baseMask, roiMask, Mat(), Point(-1, -1), dilateAmt);
		Mat fineMask = bitwiseRemoval(backgroundImGray, dslrIm);
		Mat dslrMask;
		bitwise_or(minMask, fineMask, dslrMask, roiMask);

		Mat show; dslrIm.copyTo(show, dslrMask);
		imshow("contours", show);
		Mat refIm; dslrIm.copyTo(refIm, roiMask);
		imshow("ref", refIm);
		Size imageSize = show.size();
		float screenScale = fmin((float)(screenSize.width / imageSize.width), (float)(screenSize.height / imageSize.height));
		screenScale = fmax(screenScale, 1.0f);
		cv::resizeWindow("contours", int(screenScale * imageSize.width), int(screenScale * imageSize.height));
		cv::resizeWindow("ref", int(screenScale * imageSize.width), int(screenScale * imageSize.height));

		std::cout << " Done!\n";

		if (++i == datasetImages.end()){
			i = datasetImages.begin(); // Need this to read depth image after Dslr image
		}
		if (waitKey() == 27) break;//exit on esc
	}
}

void calibrate2(String filePath){
	cout << "Reading " << filePath << "...\n";

	FileStorage cornersStorage(filePath, FileStorage::READ);
	String datasetFolder;
	cornersStorage["datasetFolder"] >> datasetFolder;
	cout << "Dataset folder: " << datasetFolder << "\n";

	FileNode cornersCollection = cornersStorage["cornersCollection"];
	FileNodeIterator i = cornersCollection.begin();
	FileNodeIterator e = cornersCollection.end();
	vector<Point3f> baseObjectPoints = initObjPoints(boardSize);
	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> dImagePoints;
	vector<vector<Point2f>> kImagePoints;
	vector<vector<float>> depthVectors;
	Size dslrSize;
	Size kinectSize;
	int count = 0;
	for (; i != e; ++i) {
		String dslrName = (*i)["fileName"];
		vector<Point2f> dslrCorners;
		(*i)["corners"] >> dslrCorners;

		if (++i == e) break;

		String kinectName = (*i)["fileName"];
		vector<Point2f> kinectCorners;
		(*i)["corners"] >> kinectCorners;
		cout << "Adding calibration data for " << dslrName << ", " << kinectName << " ...\n";

		//Reproject to Kinect Camera World Space
		fs::path dslrImName(dslrName);
		fs::path datasetFolderPath(datasetFolder);
		auto imExt = fs::extension(dslrImName);
		auto imBase = fs::basename(dslrImName);
		String depthSuffix = "_depth";
		String depthExt = ".csv";
		auto depthImName = datasetFolderPath / (imBase + depthSuffix + depthExt);
		vector<ushort> depthPoints = readCsv(depthImName.string());
		int count = 0, kinectWidth = 512, kinectHeight = 424;
		vector<float> depthSingleVector;
		for (auto c : kinectCorners) {
			// Need to flip
			unsigned i = (int(kinectWidth - c.x) + int(c.y) * kinectWidth);
			float z = depthPoints.at(i);
			depthSingleVector.push_back(z);
		}
		depthVectors.push_back(depthSingleVector);
		objectPoints.push_back(baseObjectPoints);
		dImagePoints.push_back(dslrCorners);
		kImagePoints.push_back(kinectCorners);
		if (count++ == 0) {
			Mat im1 = imread((fs::path(datasetFolder) / fs::path(dslrName)).string());
			dslrSize = im1.size();
			Mat im2 = imread((fs::path(datasetFolder) / fs::path(kinectName)).string());
			kinectSize = im2.size();
		}
	}
	cornersStorage.release();

	cout << "Done reading calibration data.\n";

	cout << "Calibrating Kinect...\n";

	Mat kinectMatrix = Mat(3, 3, CV_32FC1);
	Mat kinectDist;
	vector<Mat> kinectRvecs;
	vector<Mat> kinectTvecs;
	calibrateCamera(objectPoints, kImagePoints, kinectSize, kinectMatrix, kinectDist, kinectRvecs, kinectTvecs);


	Mat dslrMatrix = Mat(3, 3, CV_32FC1);
	Mat dslrDist;
	vector<Mat> dslrRvecs;
	vector<Mat> dslrTvecs;
	calibrateCamera(objectPoints, dImagePoints, dslrSize, dslrMatrix, dslrDist, dslrRvecs, dslrTvecs);

	// Project new object points for solving PnP
	vector<Point3f> newObjectPoints;
	vector<vector<float>>::iterator it1 = depthVectors.begin();
	for (auto v : kImagePoints) {
		vector<float>::iterator it2 = (*it1++).begin();
		for (auto p : v) {
			float z = *it2++;
			auto worldPoint = depthScreenToWorld(int(p.x), int(p.y), z, kinectMatrix, kinectDist);
			newObjectPoints.push_back(worldPoint);
		}
	}

	vector<Point2f> newDslrPoints;
	for (auto v : dImagePoints) {
		newDslrPoints.insert(newDslrPoints.end(), v.begin(), v.end());
	}

	cout << "Done calibrating.\n";

	cout << "=======================\n";
	cout << "Dslr:\n";
	cout << "Matrix :" << dslrMatrix << "\n";
	cout << "Distortion Coefficient: " << dslrDist << "\n";
	cout << "=======================\n";

	cout << "=======================\n";
	cout << "Kinect:\n";
	cout << "Matrix :" << kinectMatrix << "\n";
	cout << "Distortion Coefficient: " << kinectDist << "\n";
	cout << "=======================\n";

	cout << "Sovling PnP Ransac...\n";
	Mat dslrRvec, dslrTvec, dslrInliers;
	solvePnPRansac(newObjectPoints, newDslrPoints, dslrMatrix, dslrDist, dslrRvec, dslrTvec, false, 100, 8.0f, 100, dslrInliers);

	String calibrationFile = "calibration.yaml";

	cout << "Writing to " << calibrationFile << " ...";

	FileStorage calibStorage(calibrationFile, FileStorage::WRITE);

	calibStorage << "dslrMatrix" << dslrMatrix;
	calibStorage << "dslrDist" << dslrDist;
	calibStorage << "kinectMatrix" << kinectMatrix;
	calibStorage << "kinectDist" << kinectDist;
	calibStorage << "dslrRvec" << dslrRvec;
	calibStorage << "dslrTvec" << dslrTvec;
	cout << "DONE!\n";
	cout << "Press any key to exit ...\n";
	calibStorage.release();
	cin.ignore();
}

void calibrate(String filePath){
	cout << "Reading " << filePath << "...\n";

	FileStorage cornersStorage(filePath, FileStorage::READ);
	String datasetFolder;
	cornersStorage["datasetFolder"] >> datasetFolder;
	cout << "Dataset folder: " << datasetFolder << "\n";

	FileNode cornersCollection = cornersStorage["cornersCollection"];
	FileNodeIterator i = cornersCollection.begin();
	FileNodeIterator e = cornersCollection.end();
	vector<Point3f> baseObjectPoints = initObjPoints(boardSize);
	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> dImagePoints;
	vector<vector<Point2f>> kImagePoints;
	Size dslrSize;
	Size kinectSize;
	int count = 0;
	for (; i != e; ++i) {
		String dslrName = (*i)["fileName"];
		vector<Point2f> dslrCorners;
		(*i)["corners"] >> dslrCorners;

		if (++i == e) break;

		String kinectName = (*i)["fileName"];
		vector<Point2f> kinectCorners;
		(*i)["corners"] >> kinectCorners;
		cout << "Adding calibration data for " << dslrName << ", " << kinectName << " ...\n";
		objectPoints.push_back(baseObjectPoints);
		dImagePoints.push_back(dslrCorners);
		kImagePoints.push_back(kinectCorners);
		if (count++ == 0) {
			Mat im1 = imread((fs::path(datasetFolder) / fs::path(dslrName)).string());
			dslrSize = im1.size();
			Mat im2 = imread((fs::path(datasetFolder) / fs::path(kinectName)).string());
			kinectSize = im2.size();
		}
	}
	cornersStorage.release();

	cout << "Done reading calibration data.\n";

	cout << "Calibrating Dslr and Kinect...\n";


	Mat dslrMatrix = Mat(3, 3, CV_32FC1);
	Mat dslrDist;
	vector<Mat> dslrRvecs;
	vector<Mat> dslrTvecs;

	Mat kinectMatrix = Mat(3, 3, CV_32FC1);
	Mat kinectDist;
	vector<Mat> kinectRvecs;
	vector<Mat> kinectTvecs;

	calibrateCamera(objectPoints, dImagePoints, dslrSize, dslrMatrix, dslrDist, dslrRvecs, dslrTvecs);
	calibrateCamera(objectPoints, kImagePoints, kinectSize, kinectMatrix, kinectDist, kinectRvecs, kinectTvecs);

	cout << "Done calibrating.\n";

	cout << "=======================\n";
	cout << "Dslr:\n";
	cout << "Matrix :" << dslrMatrix << "\n";
	cout << "Distortion Coefficient: " << dslrDist << "\n";
	cout << "=======================\n";

	cout << "=======================\n";
	cout << "Kinect:\n";
	cout << "Matrix :" << kinectMatrix << "\n";
	cout << "Distortion Coefficient: " << kinectDist << "\n";
	cout << "=======================\n";

	String calibrationFile = "calibration.yaml";

	cout << "Writing to " << calibrationFile << " ...";

	FileStorage calibStorage(calibrationFile, FileStorage::WRITE);

	calibStorage << "dslrMatrix" << dslrMatrix;
	calibStorage << "dslrDist" << dslrDist;
	calibStorage << "kinectMatrix" << kinectMatrix;
	calibStorage << "kinectDist" << kinectDist;
	cout << "DONE!\n";
	cout << "Press any key to exit ...\n";
	calibStorage.release();
	cin.ignore();
}


int main() {
	//extractCorners();
	//calibrate2("corners.yaml");
	//filterDot();
	//filterContour();
	//filterBackground();
	filterCombine();
	return 1;
}